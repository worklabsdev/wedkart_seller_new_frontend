import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "./services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "app";
  isLogin = true;

  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private router: Router
  ) {}

  ngOnInit() {
    if (this.cookies.get("sellerToken")) {
      this.api.get("/api/v1/seller/confirm-token").subscribe(
        (res: Response) => {
          if (res["status"].toString() == "success") {
            // this.router.navigate(["/dashboard"]);
          } else {
            this.router.navigate(["/"]);
            this.toast.error("Please Login Firstly");
            this.cookies.delete("sellerToken");
            this.cookies.delete("sellerId");
            this.cookies.delete("sellerEmail");
          }
        },
        (res: Response) => {
          console.log(res);
        }
      );
    }
  }
}
