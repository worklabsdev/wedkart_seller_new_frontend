import { Component, OnInit } from '@angular/core';
import { NavService } from '../nav-service.service';

@Component({
  selector: 'app-trash',
  templateUrl: './trash.component.html',
  styleUrls: ['./trash.component.css']
})
export class TrashComponent implements OnInit {

  constructor( private nav:NavService ) { }

  ngOnInit() {
    this.nav.setPath({'path':'messages', breadcrumbs:[
      {name:'Dashboard', path:'/dashboard'},
      {name:'Trash'} 
    ]})
		window.scrollTo(0,0);
  }

}
