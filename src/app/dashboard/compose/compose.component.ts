import { Component, OnInit } from '@angular/core';
import { NavService } from '../nav-service.service';

@Component({
  selector: 'app-compose',
  templateUrl: './compose.component.html',
  styleUrls: ['./compose.component.css']
})
export class ComposeComponent implements OnInit {

  constructor( private nav:NavService ) { }

  ngOnInit() {
    this.nav.setPath({'path':'messages', breadcrumbs:[
      {name:'Dashboard', path:'/dashboard'},
      {name:'Compose Message'} 
    ]})
		window.scrollTo(0,0);
  }

}
