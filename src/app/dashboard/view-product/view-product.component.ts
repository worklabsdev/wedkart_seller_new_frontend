import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { NavService } from "../nav-service.service";
import { environment } from "../../../environments/environment";
declare var jQuery: any;
declare var $: any;

@Component({
  selector: "app-view-product",
  templateUrl: "./view-product.component.html",
  styleUrls: ["./view-product.component.css"]
})
export class ViewProductComponent implements OnInit {
  currProduct: any = {};
  mainImg = "";
  delIndex = -1;
  speckey: any = "";
  specval: any = "";
  deleteSpec: any = {};
  public image: any;
  baseurl: any;
  stopAdd: any = 1;
  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private route: ActivatedRoute,
    private nav: NavService,
    private router: Router
  ) {
    this.baseurl = this.api.baseurl;
  }
  public environment = environment;

  ngOnInit() {
    this.nav.setPath({
      path: "products",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Products", path: "/dashboard/products" },
        { name: "View product" }
      ]
    });

    window.scrollTo(0, 0);
    this.route.params.subscribe(params => {
      var id = params["id"]; // (+) converts string 'id' to a number
      this.getProduct(id);
    });
  }

  public showMain(img) {
    this.mainImg = img;
  }

  public openDeleteImage(index) {
    this.delIndex = index;
    $("#delImageModal").modal({ show: "true" });
    $("#delImageModal").appendTo("body");
  }

  public openChangeImage() {
    $("#changeImageModal").modal({ show: "true" });
    $("#changeImageModal").appendTo("body");
  }

  public openAddImage() {
    $("#addImageModal").modal({ show: "true" });
    $("#addImageModal").appendTo("body");
  }

  updateImage(files: any) {
    this.currProduct["tempPic"] = files.item(0);
  }

  ChangeProductImage() {
    var product_img_data = new FormData();
    product_img_data.append("productImage", this.currProduct["tempPic"]);
    //product_img_data.append("id", this.currProduct["_id"]);
    this.api
      .post(
        "/api/v1/seller/product/" + this.currProduct["_id"] + "/main-image",
        product_img_data
      )
      .subscribe(
        (res: Response) => {
          this.toast.success("Product Image Updated Succesfully");
          this.currProduct["mainImage"] = res["data"]["url"];
          // this.currProduct["mainImage"] = res["data"]["mainImage"];
          //this.mainImg = this.currProduct["mainImage"];
          $("#changeImageModal").modal("toggle");
        },
        (res: Response) => {
          console.log(res);
          this.toast.error("File Too Large");
        }
      );
  }

  AddProductImage() {
    var product_img_data = new FormData();
    product_img_data.append("productImage", this.currProduct["tempPic"]);
    product_img_data.append("id", this.currProduct["_id"]);
    this.api
      .post(
        "/api/v1/seller/product/" + this.currProduct._id + "/image",
        product_img_data
      )
      .subscribe(
        (res: Response) => {
          this.toast.success("Product Image Added Succesfully");
          this.getProduct(this.currProduct["_id"]);
          $("#addImageModal").modal("toggle");
        },
        (res: Response) => {
          console.log(res);
          this.toast.error("File Too Large");
        }
      );
  }

  public deleteProductImg() {
    this.api
      .post(
        "/api/v1/seller/product/" +
          this.currProduct._id +
          "/image/" +
          this.delIndex,
        { id: this.currProduct["_id"] }
      )
      .subscribe(
        (res: Response) => {
          if (res["status"].toString() == "success") {
            this.toast.success("Image Successfully Deleted");
            this.getProduct(this.currProduct["_id"]);
            $("#delImageModal").modal("toggle");
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (res: Response) => {
          console.log(res);
          if (res["error"].message == "Errors") {
            this.toast.error(res["error"].data.errors[0].msg);
          } else {
            this.toast.error(res["error"].message);
          }
        }
      );
  }
  public getProduct(id) {
    this.api.get("/api/v1/seller/product/" + id).subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          // res["data"]["mainImage"] = res["data"]["mainImage"];
          // this.mainImg = res["data"]["mainImage"];
          // for (let i = 0; i < res["data"]["images"].length; i++) {
          //   res["data"]["images"][i] = res["data"]["images"][i];
          // }
          console.log(res["data"]);

          this.currProduct = res["data"];
          if (this.currProduct.images.length == 3) {
            this.stopAdd = "";
          }
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }

  public openSpecModal() {
    $("#specModal").modal({ show: "true" });
    $("#specModal").appendTo("body");
  }

  public openDeletSpeceModel(spec) {
    this.deleteSpec = spec;
    $("#specDelModal").modal({ show: "true" });
    $("#specDelModal").appendTo("body");
  }

  public addSpec() {
    this.api
      .post("/api/v1/seller/product/" + this.currProduct._id + "/spec/", {
        id: this.currProduct["_id"],
        skey: this.speckey,
        sval: this.specval
      })
      .subscribe(
        (res: Response) => {
          if (res["status"].toString() == "success") {
            this.toast.success("Specification Successfully Added");
            this.getProduct(this.currProduct["_id"]);
            $("#specModal").modal("toggle");
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (res: Response) => {
          if (res["error"].message == "Errors") {
            this.toast.error(res["error"].data.errors[0].msg);
          } else {
            this.toast.error(res["error"].message);
          }
        }
      );
  }

  public deleteProductSpec() {
    this.api
      .post("/api/v1/seller/product/" + this.currProduct._id + "/spec/remove", {
        id: this.currProduct["_id"],
        sid: this.deleteSpec["id"]
      })
      .subscribe(
        (res: Response) => {
          if (res["status"].toString() == "success") {
            this.toast.success("Specification Successfully Deleted");
            this.getProduct(this.currProduct["_id"]);
            $("#specDelModal").modal("toggle");
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (res: Response) => {
          if (res["error"].message == "Errors") {
            this.toast.error(res["error"].data.errors[0].msg);
          } else {
            this.toast.error(res["error"].message);
          }
        }
      );
  }
}
