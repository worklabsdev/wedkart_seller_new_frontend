import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { NavService } from "../nav-service.service";
import { environment } from '../../../environments/environment';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  totalProd: number = 0;
  ordersLength: number = 0;
  compOrdersLength: number = 0;
  totalSales: number = 0;
  currProduct: any = {};
  latestOrders = [];
  completeOrders = [];
  latest: any = "";
  showMsg: any = "";
  latestMessages = [];
  public analytics:any;
  public environment = environment;

  public chartLabels:string[];
  public chartData:any[]=[{
    label: "Sales",
    data: [0,0,0,0,0,0,0,0,0,0,0,0]        
  },{
    label: "Earnings",
    data: [0,0,0,0,0,0,0,0,0,0,0,0]        
  }];
  public chartType:string = 'bar';
  public chartOptions:any = {'backgroundColor': [
               "#FF6384",
            "#4BC0C0",
            "#FFCE56",
            "#E7E9ED",
            "#36A2EB"
            ]}


  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private router: Router
  ) {}
  
  ngOnInit() {
    
    this.nav.setPath({
      path: "overview",
      breadcrumbs: [{ name: "Dashboard" }]
    });
    window.scrollTo(0, 0);
    this.getProdQuantity();
    this.getOrdersQty();
    this.getCompleteOrders();
    this.getRecentMessages();
    this.getAnalytics(); 
  }

  chartHovered(e){
    console.log(e);
  }

  chartClicked(e){
    console.log(e);
  }

  getAnalytics(){
    this.api.get("/api/v1/seller/analytics").subscribe(
      (res: any) => {
      console.log(res);
      this.analytics = res.data;
      this.chartLabels = res.data.timeline.map((item: any)=>{
          return `${ item.month } / ${ item.year }`;
      });
      

      let count = res.data.timeline.map((item: any)=>{
        return item.count;
      });
      let earnings = res.data.timeline.map((item: any)=>{
        if(!item.earnings){
          return 0;
        }else{
          return item.earnings;
        }
        
      });

      this.chartData[0].data = count;
      this.chartData[1].data = earnings;
    },

      (res: Response) => {
        console.log(res);
      }
    );
  }

  // getRecentMessages(){
  //   this.api.get("/api/v1/seller/messages").subscribe(
  //     (res: any) => {
  //     console.log(res);
  //     //this.recentMessages = res.data;
  //     },
  //     (res: Response) => {
  //       console.log(res);
  //     }
  //   );
  // }



  public getProdQuantity() {
    this.api.get("/api/v1/seller/products").subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          for (var i = 0; i < res["data"].length; i++) {
            this.totalProd += res["data"][i].quantity;
          }
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }

  public getOrdersQty() {
    this.api.post("/api/v1/seller/orders", {}).subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          if (res["data"].length) {
            this.latestOrders = [];
            this.ordersLength = res["data"].length;
            for (var i = 0; i < res["data"].length; i++) {
              if (i < 5) {
                this.latestOrders.push(res["data"][i]);
              }
            }
            if (this.ordersLength > 0) {
              this.latest = 1;
            }
          }
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }

  public getCompleteOrders() {
    this.api.post("/api/v1/seller/orders", { status: "Completed" }).subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          if (res["data"].length) {
            this.completeOrders = res["data"];
            this.compOrdersLength = this.completeOrders.length;
            Array.prototype.forEach.call(this.completeOrders, element => {
              for (var i = 0; i < element.products.length; i++) {
                this.totalSales +=
                  element.products[i].quantity *
                  element.products[i].product_id.dprice *
                  ((100 - element.products[i].product_id.commission) / 100);
              }
            });
          }
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }

  getRecentMessages() {
    if (this.cookies.get("sellerToken")) {
      this.api.get("/api/v1/seller/messages").subscribe(
        (res: any) => {
          console.log(res);
          if (res["status"].toString() == "success") {
            if (res["data"].length != 0) {
              this.latestMessages = res.data;
              for (var i = 0; i < this.latestMessages.length; i++) {
                var date = new Date(this.latestMessages[i]["create_time"]);
                var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
                var day = ("0" + date.getDate()).slice(-2);
                var hours = ("0" + date.getHours()).slice(-2);
                var minutes = ("0" + date.getMinutes()).slice(-2);
                this.latestMessages[i]["create_time"] =
                  [day, mnth, date.getFullYear()].join("-") +
                  "  " +
                  [hours, minutes].join(":");
              }
              this.showMsg = 1;
            }
          }
        },
        (res: Response) => {
          console.log(res);
        }
      );
    }
  }
}
