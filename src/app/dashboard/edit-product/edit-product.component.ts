import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { NavService } from "../nav-service.service";

@Component({
  selector: "app-edit-product",
  templateUrl: "./edit-product.component.html",
  styleUrls: ["./edit-product.component.css"]
})
export class EditProductComponent implements OnInit {
  currProduct: any = {};
  sizes: any = [];
  services: any = [];
  colors: any = [];
  public color: any;
  categories: any = [];
  public subCategories: any = [];
  public category: any = "";
  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private route: ActivatedRoute,
    private nav: NavService,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "products",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Products", path: "/dashboard/products" },
        { name: "Edit product" }
      ]
    });
    window.scrollTo(0, 0);
    this.route.params.subscribe(params => {
      var id = params["id"]; // (+) converts string 'id' to a number
      this.getProduct(id);
    });
    //this.getAllCategories();
  }

  /* private getAllCategories() {
    console.log("getAllCategories");
    this.api.get("/api/v1/seller/getCategory").subscribe(
      (res: Response) => {
        console.log("category", res);
        if (res["status"].toString() == "success") {
          this.categories = res["data"];
        } else {
          this.categories = [];
          this.toast.error("Some Problem Occured");
        }
      },
      (err: Response) => {
        console.log(err);
        if (err["error"].message == "Errors") {
          this.toast.error(err["error"].data.errors[0].msg);
        } else {
          this.toast.error(err["error"].message);
        }
      }
    );
  }

  selectSubCategory(checkVal) {
    console.log("something", checkVal);
    this.api.get("/api/v1/seller/getsubcategory/" + checkVal).subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          this.subCategories = res["data"].subCategory;
        } else {
          this.subCategories = [];
          this.toast.error("Some Problem Occured");
        }
      },
      (err: Response) => {
        console.log(err);
        if (err["error"].message == "Errors") {
          this.toast.error(err["error"].data.errors[0].msg);
        } else {
          this.toast.error(err["error"].message);
        }
      }
    );

    for (var i = 0; i < this.categories.length; i++) {
      if (this.categories[i]._id == checkVal) {
        this.category = this.categories[i].name;
      }
      console.log(this.category);
    }
  }
 */
  public getProduct(id) {
    this.api.get("/api/v1/seller/product/" + id).subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          // res["data"].map(obj => {
          //   obj["mainImage"] = this.api.serverurl + obj["mainImage"];
          // });
          this.currProduct = res["data"];
          console.log("this is curr data", this.currProduct);
          this.colors = this.currProduct.color;
          this.sizes = this.currProduct.size;
          this.services = this.currProduct.service;
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }

  selectSize(checkVal) {
    var valIndex = this.sizes.indexOf(checkVal);
    if (valIndex == -1) {
      this.sizes.push(checkVal);
    } else {
      this.sizes.splice(valIndex, 1);
    }
    console.log(this.sizes);
  }

  selectColor(checkVal) {
    var valIndex = this.colors.indexOf(checkVal);
    if (valIndex == -1) {
      this.colors.push(checkVal);
    } else {
      this.colors.splice(valIndex, 1);
    }
    console.log(this.colors);
  }

  selectServices(checkVal) {
    var valIndex = this.services.indexOf(checkVal);
    if (valIndex == -1) {
      this.services.push(checkVal);
    } else {
      this.services.splice(valIndex, 1);
    }
    console.log(this.services);
  }

  editProduct() {
    this.currProduct["service"] = this.services;
    this.currProduct["size"] = this.sizes;
    this.currProduct["color"] = this.colors;
    this.currProduct["id"] = this.currProduct["_id"];

    this.api
      .put("/api/v1/seller/product/" + this.currProduct["id"], this.currProduct)
      .subscribe(
        (res: Response) => {
          if (res["status"].toString() == "success") {
            this.toast.success("Product Updated Succesfully");
            this.router.navigate([
              "/dashboard/view-product",
              this.currProduct.id
            ]);
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (res: Response) => {
          console.log(res);
          if (res["error"].message == "Errors") {
            this.toast.error(res["error"].data.errors[0].msg);
          } else {
            this.toast.error(res["error"].message);
          }
        }
      );
  }
}
