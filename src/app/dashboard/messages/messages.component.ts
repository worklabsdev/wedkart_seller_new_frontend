import { Component, OnInit } from "@angular/core";
import { NavService } from "../nav-service.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";

declare var jQuery: any;
declare var $: any;
import "rxjs";
@Component({
  selector: "app-messages",
  templateUrl: "./messages.component.html",
  styleUrls: ["./messages.component.css"]
})
export class MessagesComponent implements OnInit {
  messages = [];
  showMsg: any = "";
  constructor(
    private nav: NavService,
    public apis: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "messages",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Messages" }
      ]
    });
    window.scrollTo(0, 0);
    this.getMessages();
  }

  getMessages() {
    if (this.cookies.get("sellerToken")) {
      this.apis.get("/api/v1/seller/messages").subscribe(
        (res: Response) => {
          if (res["status"].toString() == "success") {
            if (res["data"].length != 0) {
              this.messages = res["data"];
              this.showMsg = 1;
              for (var i = 0; i < this.messages.length; i++) {
                var date = new Date(this.messages[i]["create_time"]);
                console.log(date);
                var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
                var day = ("0" + date.getDate()).slice(-2);
                var hours = ("0" + date.getHours()).slice(-2);
                var minutes = ("0" + date.getMinutes()).slice(-2);
                this.messages[i]["create_time"] =
                  [day, mnth, date.getFullYear()].join("-") +
                  "  " +
                  [hours, minutes].join(":");
              }
            }
          }
        },
        (res: Response) => {
          console.log(res);
        }
      );
    }
  }
}
