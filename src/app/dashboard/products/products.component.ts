import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { NavService } from "../nav-service.service";
import { environment } from "../../../environments/environment";

declare var jQuery: any;
declare var $: any;

@Component({
  selector: "app-products",
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.css"]
})
export class ProductsComponent implements OnInit {
  products: any = [];
  prodList: any = "";
  currProduct: any = {};
  public p: number = 1;
  public environment = environment;
  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "products",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Products" }
      ]
    });
    window.scrollTo(0, 0);
    this.getProducts();
  }

  public openDeleteModel(content, product) {
    this.currProduct = product;
    $("#delProductModal").modal({ show: "true" });
    $("#delProductModal").appendTo("body");
  }

  public deleteProduct(id) {
    this.api.delete("/api/v1/seller/product/" + id).subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          this.toast.success("Product Successfully Deleted");
          this.getProducts();
          $("#delProductModal").modal("toggle");
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }

  public getProducts() {
    this.api.get("/api/v1/seller/products").subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          console.log("response");
          console.log(res);
          // res["data"].map(obj => {
          //   obj["mainImage"]
          //     ? (obj["mainImage"] =
          //         this.api.baseurl + "/uploads/" + obj["mainImage"])
          //     : (obj["mainImage"] = "assets/img/products/THUMB");
          // });
          this.products = res["data"];
          if (this.products.length != 0) {
            this.prodList = 1;
          } else {
            this.prodList = "";
          }
          console.log(this.products);
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }
}
