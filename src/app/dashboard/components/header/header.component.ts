import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { ApiService } from "../../../services/api.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  seller: any = {};
  constructor(
    public api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private router: Router
  ) {}

  ngOnInit() {
    this.details();
  }
  details() {
    this.api.get("/api/v1/seller/details").subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          this.seller = res["data"];
          this.api.sellerData = this.seller;
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }
  public doLogout() {
    this.api.post("/api/v1/seller/logout", {}).subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          this.cookies.delete("sellerToken");
          this.cookies.delete("sellerId");
          this.cookies.delete("sellerEmail");
          this.toast.success("Logged Out");
          this.router.navigate(["/login"]);
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }
}
