import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NavService } from "../../nav-service.service";
import { ApiService } from "../../../services/api.service";
import { CookieService } from "ngx-cookie-service";
@Component({
  selector: "app-navigation",
  templateUrl: "./navigation.component.html",
  styleUrls: ["./navigation.component.css"]
})
export class NavigationComponent implements OnInit {
  public active: any;
  constructor(
    private route: ActivatedRoute,
    private nav: NavService,
    private cookies: CookieService,
    private router: Router,
    public apis: ApiService
  ) {}

  ngOnInit() {
    this.nav.currPath.subscribe(data => {
      this.active = data["path"];
      console.log(this.active);
    });
    this.msgNotification();
  }

  msgNotification() {
    if (this.cookies.get("sellerToken")) {
      this.apis.get("/api/v1/seller/notification").subscribe(
        (res: Response) => {
          if (res["status"].toString() == "success") {
            // console.log("Notification");
            // console.log(res);
            this.apis.notification = res["data"];
          }
        },
        (res: Response) => {
          console.log(res);
        }
      );
    }
  }
}
