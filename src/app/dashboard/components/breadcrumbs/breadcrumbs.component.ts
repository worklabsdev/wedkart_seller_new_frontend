import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavService } from '../../nav-service.service'
@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.css']
})
export class BreadcrumbsComponent implements OnInit {
  breadcrumbs = "Dashboard"
  constructor(private nav: NavService) { }

  ngOnInit() {
    this.nav.currPath.subscribe((data)=>{
      this.breadcrumbs = data['breadcrumbs']
    })
  }

}
