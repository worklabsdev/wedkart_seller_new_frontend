import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { NavService } from "../nav-service.service";
declare var jQuery: any;
declare var $: any;
import "rxjs";
import { environment } from "../../../environments/environment";
@Component({
  selector: "app-orders",
  templateUrl: "./orders.component.html",
  styleUrls: ["./orders.component.css"]
})
export class OrdersComponent implements OnInit {
  public allOrders = [];
  public currOrderId: any = "";
  public order = [];
  public list: any = "";
  public status: any = "";
  public nonCompOrders = [];
  public compOrders = [];
  public cancelOrders = [];
  public p: number = 1;
  public q: number = 1;
  public r: number = 1;
  public s: number = 1;
  public environment = environment;
  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "orders",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Orders" }
      ]
    });
    window.scrollTo(0, 0);
    this.getOrders();
    this.getCompOrders();
    this.getNonCompOrders();
    this.getCancelOrders();
  }

  selectStatus(checkVal) {
    this.status = checkVal;
  }

  public openDeleteModel(content, id) {
    this.currOrderId = id;
    console.log("currOrderId");
    console.log(this.currOrderId);
    $("#orderstatus_modal").modal({ show: true });
    $("#orderstatus_modal").appendTo("body");
  }

  public getOrders() {
    this.allOrders = [];
    this.api.post("/api/v1/seller/orders", {}).subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          this.allOrders = res["data"];
          if (this.allOrders.length != 0) {
            this.list = 1;
          } else {
            this.list = "";
          }
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }

  public getCompOrders() {
    this.allOrders = [];
    this.api.post("/api/v1/seller/orders", { status: "Completed" }).subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          this.compOrders = res["data"];
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }

  public getNonCompOrders() {
    this.allOrders = [];
    this.api.post("/api/v1/seller/orders", { status: "Pending" }).subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          this.nonCompOrders = res["data"];
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }

  public getCancelOrders() {
    this.allOrders = [];
    this.api.post("/api/v1/seller/orders", { status: "Cancelled" }).subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          this.cancelOrders = res["data"];
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }

  // public getOrderDetails(id) {
  //   this.order = [];
  //   this.api.get("/api/v1/seller/order" + id).subscribe(
  //     (res: Response) => {
  //       console.log(res);
  //       if (res["status"].toString() == "success") {
  //         this.order = res["data"];
  //       } else {
  //         this.toast.error("Some Problem Occured");
  //       }
  //     },
  //     (res: Response) => {
  //       console.log(res);
  //     }
  //   );
  // }

  public changeOrderStatus() {
    if (this.status == "") {
      this.toast.error("Please select a status");
    } else {
      this.api
        .post("/api/v1/seller/change-order-status", {
          id: this.currOrderId,
          status: this.status
        })
        .subscribe(
          (res: Response) => {
            if (res["status"].toString() == "success") {
              this.toast.success("Status updated");
              this.getOrders();
              this.getCompOrders();
              this.getNonCompOrders();
              this.getCancelOrders();
            } else {
              this.toast.error("Some Problem Occured");
            }
          },
          (res: Response) => {
            console.log(res);
          }
        );
    }
  }
}
