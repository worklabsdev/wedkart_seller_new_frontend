import { Component, OnInit } from "@angular/core";
import { NavService } from "../nav-service.service";
import { Router } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
@Component({
  selector: "app-bank-account-information",
  templateUrl: "./bank-account-information.component.html",
  styleUrls: ["./bank-account-information.component.css"]
})
export class BankAccountInformationComponent implements OnInit {
  sellerBank: any = {};

  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private router: Router
  ) {}
  ngOnInit() {
    this.nav.setPath({
      path: "account-information",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Bank Account Information" }
      ]
    });
    window.scrollTo(0, 0);
    this.getBankDetails();
  }

  public getBankDetails() {
    this.api.get("/api/v1/seller/details").subscribe(
      (res: Response) => {
        console.log(res);
        console.log("vikki");
        if (res["status"].toString() == "success") {
          console.log(res["data"]);
          //	res['data'].image ? res['data'].image =  this.api.serverurl + res['data'].image : res['data'].image = 'assets/img/admin.png'

          this.sellerBank = res["data"];
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }

  doUpdateBank() {
    this.api.put("/api/v1/seller/edit-bank-details", this.sellerBank).subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          this.toast.success("Bank Details Successfully Updated");
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }
}
