import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { NavService } from "../nav-service.service";

@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.css"]
})
export class ChangePasswordComponent implements OnInit {
  oldPassword: any = null;
  newPassword: any = null;
  conPassword: any = null;

  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "account-information",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Change Password" }
      ]
    });
    window.scrollTo(0, 0);
  }

  doChangePassword() {
    this.api
      .put("/api/v1/seller/change-password", {
        opassword: this.oldPassword,
        npassword: this.newPassword
      })
      .subscribe(
        (res: Response) => {
          if (res["status"].toString() == "success") {
            this.toast.success("Password Successfully changed");
            this.router.navigate(["/dashboard"]);
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (res: Response) => {
          if (res["error"].message == "Errors") {
            this.toast.error(res["error"].data.errors[0].msg);
          } else {
            this.toast.error(res["error"].message);
          }
        }
      );
  }
}
