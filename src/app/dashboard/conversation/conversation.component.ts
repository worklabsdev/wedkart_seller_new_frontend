import { Component, OnInit } from "@angular/core";
import { NavService } from "../nav-service.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";

declare var jQuery: any;
declare var $: any;
import "rxjs";
import { elementEventFullName } from "@angular/compiler/src/view_compiler/view_compiler";

@Component({
  selector: "app-conversation",
  templateUrl: "./conversation.component.html",
  styleUrls: ["./conversation.component.css"]
})
export class ConversationComponent implements OnInit {
  public messages = [];
  public message = "";
  buyerId = "";
  productId = "";
  product: any = {};
  loadAll: any = "";
  constructor(
    private nav: NavService,
    private route: ActivatedRoute,
    private cookies: CookieService,
    private router: Router,
    /* private spinner:NgxSpinnerService, */
    public apis: ApiService,
    private toast: ToastrService
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "messages",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Messages" }
      ]
    });
    // window.scrollTo(0, 0);
    this.route.params.subscribe(params => {
      var buyer = params["buyer"];
      var product = params["product"];
      this.buyerId = buyer;
      this.productId = product;
      this.getSingleConvo(this.buyerId, this.productId);
      this.msgSeen(this.buyerId);
    });
  }

  getSingleConvo(buyer, product) {
    if (this.cookies.get("sellerToken")) {
      this.apis
        .post("/api/v1/seller/singleConvo", {
          buyerId: buyer,
          productId: product
        })
        .subscribe(
          (res: Response) => {
            if (res["status"].toString() == "success") {
              this.messages = res["data"].slice(-4);
              this.loadAll = "";
              for (var i = 0; i < this.messages.length; i++) {
                var date = new Date(this.messages[i]["create_time"]);
                var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
                var day = ("0" + date.getDate()).slice(-2);
                var hours = ("0" + date.getHours()).slice(-2);
                var minutes = ("0" + date.getMinutes()).slice(-2);
                this.messages[i]["create_time"] =
                  [day, mnth, date.getFullYear()].join("-") +
                  "  " +
                  [hours, minutes].join(":");
              }
              if (this.messages[this.messages.length - 1]) {
                this.product = this.messages[
                  this.messages.length - 1
                ].productId;
                this.productId = this.product._id;
              }
            }
          },
          (res: Response) => {
            console.log(res);
          }
        );
    }
  }
  showAll(buyer, product) {
    if (this.cookies.get("sellerToken")) {
      this.apis
        .post("/api/v1/seller/singleConvo", {
          buyerId: buyer,
          productId: product
        })
        .subscribe(
          (res: Response) => {
            if (res["status"].toString() == "success") {
              this.messages = res["data"];
              this.loadAll = 1;
              for (var i = 0; i < this.messages.length; i++) {
                var date = new Date(this.messages[i]["create_time"]);
                var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
                var day = ("0" + date.getDate()).slice(-2);
                var hours = ("0" + date.getHours()).slice(-2);
                var minutes = ("0" + date.getMinutes()).slice(-2);
                this.messages[i]["create_time"] =
                  [day, mnth, date.getFullYear()].join("-") +
                  "  " +
                  [hours, minutes].join(":");
              }
              if (this.messages[this.messages.length]) {
                this.product = this.messages[this.messages.length].productId;
                this.productId = this.product._id;
              }
            }
          },
          (res: Response) => {
            console.log(res);
          }
        );
    }
  }

  sendMessage(formData) {
    if (!formData.message.trim()) {
      document.getElementById("btn_submit").style.display = "hidden";
      this.toast.error("Message Can't Be Empty");
    } else {
      document.getElementById("btn_submit").style.display = "block";

      if (this.cookies.get("sellerToken")) {
        this.apis
          .post("/api/v1/seller/message", {
            message: formData["message"],
            buyerId: this.buyerId,
            productId: this.productId
          })
          .subscribe(
            (res: Response) => {
              if (res["status"].toString() == "success") {
                this.getSingleConvo(this.buyerId, this.productId);
                this.message = "";
                // this.toast.success("Sent");
              }
            },
            (res: Response) => {
              console.log(res);
            }
          );
      }
    }
  }
  msgSeen(buyer) {
    if (this.cookies.get("sellerToken")) {
      this.apis.get("/api/v1/seller/msgSeen/" + buyer).subscribe(
        (res: Response) => {
          if (res["status"].toString() == "success") {
            console.log("msgSeen");
          }
        },
        (res: Response) => {
          console.log(res);
        }
      );
    }
  }
}
