import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

// global components
import { HeaderComponent } from "./components/header/header.component";
import { NavigationComponent } from "./components/navigation/navigation.component";
import { BreadcrumbsComponent } from "./components/breadcrumbs/breadcrumbs.component";
import { FooterComponent } from "./components/footer/footer.component";

// routing components
import { HomeComponent } from "./home/home.component";
import { ChangePasswordComponent } from "./change-password/change-password.component";
import { UpdateProfileComponent } from "./update-profile/update-profile.component";
import { ProductsComponent } from "./products/products.component";
import { AddProductComponent } from "./add-product/add-product.component";
import { EditProductComponent } from "./edit-product/edit-product.component";
import { AccountInformationComponent } from "./account-information/account-information.component";
import { BankAccountInformationComponent } from "./bank-account-information/bank-account-information.component";
import { MessagesComponent } from "./messages/messages.component";
import { SentMailsComponent } from "./sent-mails/sent-mails.component";
import { TrashComponent } from "./trash/trash.component";
import { ComposeComponent } from "./compose/compose.component";

// services
import { HttpClientModule } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { ToastrModule } from "ngx-toastr";
import { ApiService } from "../services/api.service";
import { NavService } from "./nav-service.service";

import { ColorPickerModule } from "ngx-color-picker";
import {
  BsDatepickerModule,
  TimepickerModule,
  ModalModule
} from "ngx-bootstrap";
import { ViewProductComponent } from "./view-product/view-product.component";
import { LoginConfirmComponent } from "./login-confirm/login-confirm.component";
import { ImageCropperModule } from "ngx-image-cropper";
import { OrdersComponent } from "./orders/orders.component";
import { SoldComponent } from "./sold/sold.component";
import { ShopComponent } from "./shop/shop.component";
import { AddShopComponent } from "./add-shop/add-shop.component";
import { ViewShopComponent } from "./view-shop/view-shop.component";
import { EditShopComponent } from "./edit-shop/edit-shop.component";
import { CancelRequestsComponent } from "./cancel-requests/cancel-requests.component";
import { ViewOrderComponent } from "./view-order/view-order.component";
import { ConversationComponent } from "./conversation/conversation.component";
import { NgxPaginationModule } from "ngx-pagination";
import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [
    HeaderComponent,
    NavigationComponent,
    BreadcrumbsComponent,
    FooterComponent,
    BankAccountInformationComponent,
    HomeComponent,
    ChangePasswordComponent,
    UpdateProfileComponent,
    ProductsComponent,
    AddProductComponent,
    EditProductComponent,
    AccountInformationComponent,
    ViewProductComponent,
    MessagesComponent,
    SentMailsComponent,
    TrashComponent,
    ComposeComponent,
    LoginConfirmComponent,
    OrdersComponent,
    SoldComponent,
    ShopComponent,
    AddShopComponent,
    ViewShopComponent,
    EditShopComponent,
    CancelRequestsComponent,
    ViewOrderComponent,
    ConversationComponent
  ],
  imports: [
    NgxPaginationModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ColorPickerModule,
    RouterModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    ModalModule.forRoot(),
    ToastrModule.forRoot(),
    ImageCropperModule,
    ChartsModule
  ],
  exports: [
    HeaderComponent,
    NavigationComponent,
    BreadcrumbsComponent,
    FooterComponent
  ],
  providers: [CookieService, ApiService, NavService],
  bootstrap: []
})
export class DashboardModule {}
