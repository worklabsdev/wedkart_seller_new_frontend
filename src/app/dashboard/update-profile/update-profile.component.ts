import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { NavService } from "../nav-service.service";

@Component({
  selector: "app-update-profile",
  templateUrl: "./update-profile.component.html",
  styleUrls: ["./update-profile.component.css"]
})
export class UpdateProfileComponent implements OnInit {
  seller: any = {};
  profilePicBtn: Boolean = false;
  profilePic: any = "";
  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "overview",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Update Profile" }
      ]
    });
    window.scrollTo(0, 0);
    this.getDetails();
  }

  public getDetails() {
    this.api.get("/api/v1/seller/details").subscribe(
      (res: Response) => {
        console.log(res);
        console.log("vikki");
        if (res["status"].toString() == "success") {
          console.log(res["data"]);
          //	res['data'].image ? res['data'].image =  this.api.serverurl + res['data'].image : res['data'].image = 'assets/img/admin.png'

          this.seller = res["data"];
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }

  doUpdateProfile() {
    this.api.put("/api/v1/seller/update-account", this.seller).subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          this.toast.success("Seller Successfully Updated");
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }

  updateProfileBtn(files: any) {
    this.profilePicBtn = true;
    this.profilePic = files.item(0);
  }

  updateProfileImage() {
    const formData: FormData = new FormData();
    formData.append("avtar", this.profilePic);
    this.api.put("/api/v1/seller/profile-image", formData).subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          this.profilePicBtn = false;
          //this.seller.image = res["data"].image;
          this.getDetails();
          this.toast.success("Seller Profile Pic Successfully Updated");
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }
}
