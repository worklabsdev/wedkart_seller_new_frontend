import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { NavService } from "../nav-service.service";
declare var jQuery: any;
declare var $: any;
import "rxjs";
@Component({
  selector: "app-cancel-requests",
  templateUrl: "./cancel-requests.component.html",
  styleUrls: ["./cancel-requests.component.css"]
})
export class CancelRequestsComponent implements OnInit {
  public allOrders = [];
  public order = [];
  public list: any = "";
  public orderId: any = "";
  public status: any = "";
  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "cancel-requests",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Cancellation Requests" }
      ]
    });
    window.scrollTo(0, 0);
    this.getCanelReqOrders();
  }

  // selectStatus(checkVal) {
  //   var valIndex = this.status.indexOf(checkVal);
  //   if (valIndex == -1) {
  //     this.status = checkVal;
  //   }
  // }

  public openDeleteModel(content, id) {
    this.orderId = id;
    console.log("orderId");
    console.log(this.orderId);
    $("#orderstatusmodal").modal({ show: true });
    $("#orderstatusmodal").appendTo("body");
  }

  public getCanelReqOrders() {
    this.allOrders = [];
    this.api.get("/api/v1/seller/orders/cancel-request").subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          this.allOrders = res["data"];
          if (this.allOrders.length != 0) {
            this.list = 1;
          } else {
            this.list = "";
          }
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }

  // public getOrderDetails(id) {
  //   this.order = [];
  //   this.api.get("/api/v1/seller/order" + id).subscribe(
  //     (res: Response) => {
  //       console.log(res);
  //       if (res["status"].toString() == "success") {
  //         this.order = res["data"];
  //       } else {
  //         this.toast.error("Some Problem Occured");
  //       }
  //     },
  //     (res: Response) => {
  //       console.log(res);
  //     }
  //   );
  // }

  public acceptCancelRequest() {
    this.api
      .post("/api/v1/seller/change-order-status", {
        id: this.orderId,
        status: "Cancelled"
      })
      .subscribe(
        (res: Response) => {
          console.log(res);
          if (res["status"].toString() == "success") {
            this.getCanelReqOrders();
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (res: Response) => {
          console.log(res);
        }
      );
  }
}
