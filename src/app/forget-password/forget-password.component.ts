import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service'
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  email : any = ""
  code : any = ""
  newPassword : any = ""
  conPassword : any = ""
  emailDiv : any = true
  codeDiv : any = false
  passwordDiv : any = false

  constructor(private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private router: Router) { }

  ngOnInit() {


  }


  checkEmail(){
	this.api.post('/api/v1/seller/forget-password', { email: this.email }).subscribe((res: Response) => {
			if (res['status'].toString() == 'success') {
				this.toast.success('Password Succesfully Sent on Your Email')
				this.emailDiv = false
				this.codeDiv = true
			}
			else {
				this.toast.error('Some Problem Occured')
			}
		}, (res: Response) => {
			console.log(res)
			if (res['error'].message == 'Errors') {
				this.toast.error(res['error'].data.errors[0].msg)
		}
		else {
			this.toast.error(res['error'].message)
		}
	})
  }
  
  checkCode(){
	 this.api.post('/api/v1/seller/confirm-code', { code: this.code, email: this.email }).subscribe((res: Response) => {
			if (res['status'].toString() == 'success') {
				this.toast.success('Recover Code Successfully Changed')
				this.codeDiv = false
				this.passwordDiv = true
			}
			else {
				this.toast.error('Some Problem Occured')
			}
		}, (res: Response) => {
			console.log(res)
			if (res['error'].message == 'Errors') {
				this.toast.error(res['error'].data.errors[0].msg)
		}
		else {
			this.toast.error(res['error'].message)
		}
	})
  }


  updatePassword(){
	 this.api.put('/api/v1/seller/update-password', { code: this.code, email: this.email, password: this.newPassword }).subscribe((res: Response) => {
			if (res['status'].toString() == 'success') {
				this.toast.success('Password Succesfully Changed! Please Login')
				this.router.navigate(['/login'])
			}
			else {
				this.toast.error('Some Problem Occured')
			}
		}, (res: Response) => {
			console.log(res)
			if (res['error'].message == 'Errors') {
				this.toast.error(res['error'].data.errors[0].msg)
		}
		else {
			this.toast.error(res['error'].message)
		}
	})
  }


}
