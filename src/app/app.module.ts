import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";

import { routes } from "./app.router";
import { LoginComponent } from "./login/login.component";
import { HttpClientModule } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { ToastrModule } from "ngx-toastr";
import { ApiService } from "./services/api.service";
import { DashboardModule } from "./dashboard/app.module";
import { DashboardComponent } from "./dashboard/app.component";
import { AuthGuard } from "./guards/auth.guard";
import { ForgetPasswordComponent } from "./forget-password/forget-password.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ForgetPasswordComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DashboardModule,
    RouterModule.forRoot(routes),
    ToastrModule.forRoot()
  ],
  providers: [CookieService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule {}
