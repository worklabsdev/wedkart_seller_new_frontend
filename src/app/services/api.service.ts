import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";

import { CookieService } from "ngx-cookie-service";
import { environment } from "../../environments/environment";

@Injectable()
export class ApiService {
  public baseurl: string = environment.baseurl;
  public serverurl: string = environment.serverurl;

  public notification: number = 0;
  public sellerData: any = [];
  constructor(private http: HttpClient, private cookies: CookieService) {}

  public get(url) {
    let httpOptions = {
      headers: new HttpHeaders({
        "x-auth-token": this.cookies.get("sellerToken")
      })
    };
    return this.http.get(this.baseurl + url, httpOptions);
  }

  public post(url, data) {
    let httpOptions = {
      headers: new HttpHeaders({
        "x-auth-token": this.cookies.get("sellerToken")
      })
    };
    return this.http.post(this.baseurl + url, data, httpOptions);
  }

  public put(url, data) {
    let httpOptions = {
      headers: new HttpHeaders({
        "x-auth-token": this.cookies.get("sellerToken")
      })
    };
    return this.http.put(this.baseurl + url, data, httpOptions);
  }

  public delete(url) {
    let httpOptions = {
      headers: new HttpHeaders({
        "x-auth-token": this.cookies.get("sellerToken")
      })
    };
    return this.http.delete(this.baseurl + url, httpOptions);
  }
}
